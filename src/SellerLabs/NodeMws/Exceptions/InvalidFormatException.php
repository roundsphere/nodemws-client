<?php

namespace SellerLabs\NodeMws\Exceptions;

use Exception;

/**
 * Class InvalidFormatException
 *
 * Thrown when the NodeMWS client is unable to parse a response
 * correctly into an internal entity class or representation
 *
 * @package SellerLabs\NodeMws\Exceptions
 */
class InvalidFormatException extends Exception
{

}
