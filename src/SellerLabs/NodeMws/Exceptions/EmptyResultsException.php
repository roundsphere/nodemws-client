<?php

namespace SellerLabs\NodeMws\Exceptions;

use Exception;

/**
 * Class EmptyResultsException
 *
 * Thrown when search results are empty
 *
 * @package SellerLabs\NodeMws\Exceptions
 */
class EmptyResultsException extends Exception
{

}